package com.scmitltda.crudbuilders;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CrudbuildersApplication {

	public static void main(String[] args) {
		SpringApplication.run(CrudbuildersApplication.class, args);
	}

}
