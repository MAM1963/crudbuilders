package com.scmitltda.crudbuilders.repositories;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.scmitltda.crudbuilders.domain.Cliente;

@Repository
public interface ClienteRepository extends CrudRepository<Cliente, Long> {
	
	Cliente findByNome(String nome);
	
	Cliente findByCpf(String cpf);
	
	@Query("SELECT obj FROM Cliente obj WHERE obj.nome LIKE %:nome%")
	Page<Cliente> findByNome(@Param("nome") String nome, Pageable pageRequest);
	
	@Query("SELECT obj FROM Cliente obj WHERE obj.cpf LIKE %:cpf%")
	Page<Cliente> findByCpf(@Param("cpf") String cpf, Pageable pageRequest);

	@Query("SELECT obj FROM Cliente obj WHERE obj.nome LIKE %:nome% AND obj.cpf LIKE %:cpf%")
	Page<Cliente> findByNomeAndCpf(@Param("nome") String nome, @Param("cpf") String cpf, Pageable pageRequest);
}
