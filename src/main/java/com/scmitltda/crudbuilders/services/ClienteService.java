package com.scmitltda.crudbuilders.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;

import com.scmitltda.crudbuilders.domain.Cliente;
import com.scmitltda.crudbuilders.exceptions.ClienteNotFoundException;
import com.scmitltda.crudbuilders.exceptions.CpfAlreadyExistsException;
import com.scmitltda.crudbuilders.repositories.ClienteRepository;

@Service
public class ClienteService {
	
	@Autowired
	ClienteRepository clienteRepository;
	
	public Cliente save(Cliente newCliente) {
		
		try {
			newCliente.setId(null);
			return clienteRepository.save(newCliente);
		} catch (Exception e) {
			throw new CpfAlreadyExistsException("Já existe cliente com o CPF " + 
					newCliente.getCpf() + " cadastrado.");
		}
		 
	}
	
	public Iterable<Cliente> findAll() {
		return clienteRepository.findAll();
	}
	
	public void excluirClientePorCpf(String cpf) {
		
		Cliente cliente = clienteRepository.findByCpf(cpf);
		
		if (cliente == null) {
			throw new ClienteNotFoundException("Cliente com CPF '" + cpf + "' não foi encontrado");
		}		
		
		clienteRepository.delete(cliente);
	}
	
	public Cliente altararClientePorCpf(Cliente clienteUpdate, String cpf) {
		Cliente cliente = clienteRepository.findByCpf(cpf);
		
		if (cliente == null) {
			throw new ClienteNotFoundException("Cliente com CPF '" + cpf + "' não foi encontrado");
		}		
		
		cliente.setCpf(clienteUpdate.getCpf());
		cliente.setDataNascimento(clienteUpdate.getDataNascimento());
		cliente.setNome(clienteUpdate.getNome());
				
		return clienteRepository.save(cliente);
	}
	
	public Cliente alterarNomeEDataNascimento(Cliente clienteUpdate, String cpf) {
		Cliente cliente = clienteRepository.findByCpf(cpf);
		
		if (cliente == null) {
			throw new ClienteNotFoundException("Cliente com CPF '" + cpf + "' não foi encontrado");
		}		
		
		cliente.setNome(clienteUpdate.getNome());
		cliente.setDataNascimento(clienteUpdate.getDataNascimento());
		
		return clienteRepository.save(cliente);
	}
	
	public Page<Cliente>  buscarClientePorCpf(String cpf, Integer page, 
				Integer linesPerPage, String orderBy, String direction ) {
		PageRequest pageRequest = PageRequest.of(page, linesPerPage, Direction.valueOf(direction), orderBy);
		return clienteRepository.findByCpf(cpf, pageRequest);
	}
	
	public Page<Cliente>  buscarClientePorNome(String nome, Integer page, 
			Integer linesPerPage, String orderBy, String direction ) {
	PageRequest pageRequest = PageRequest.of(page, linesPerPage, Direction.valueOf(direction), orderBy);
	return clienteRepository.findByNome(nome, pageRequest);
}
	
	public Page<Cliente>  buscarClientePorNomeECpf(String nome, String cpf, Integer page, 
			Integer linesPerPage, String orderBy, String direction ) {
	PageRequest pageRequest = PageRequest.of(page, linesPerPage, Direction.valueOf(direction), orderBy);
	return clienteRepository.findByNomeAndCpf(nome, cpf, pageRequest);

}

}
