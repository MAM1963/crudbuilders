package com.scmitltda.crudbuilders.exceptions;

public class InvalidArgumentNumberException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public InvalidArgumentNumberException(String msg) {
		super(msg);
	}

}
