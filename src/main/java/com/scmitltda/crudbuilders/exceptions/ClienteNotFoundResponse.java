package com.scmitltda.crudbuilders.exceptions;

public class ClienteNotFoundResponse {
	private String cpf;

	public ClienteNotFoundResponse(String cpf) {
		this.cpf = cpf;
	}

	public String getCpf() {
		return cpf;
	}

	public void setCpf(String cpf) {
		this.cpf = cpf;
	}
}
