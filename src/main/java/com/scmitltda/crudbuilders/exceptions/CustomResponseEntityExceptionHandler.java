package com.scmitltda.crudbuilders.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.WebRequest;


@ControllerAdvice
@RestController
public class CustomResponseEntityExceptionHandler {
	
	@ExceptionHandler
	public final ResponseEntity<Object> handleCpfAlreadyExistsException(CpfAlreadyExistsException ex, WebRequest request) {
		CpfAlreadyExistsResponse exceptionResponse = new CpfAlreadyExistsResponse(ex.getMessage());
		return new ResponseEntity<Object>(exceptionResponse, HttpStatus.BAD_REQUEST);
	}	

	@ExceptionHandler
	public final ResponseEntity<Object> handleClienteNotFoundException(ClienteNotFoundException ex, WebRequest request) {
		ClienteNotFoundResponse exceptionResponse = new ClienteNotFoundResponse(ex.getMessage());
		return new ResponseEntity<Object>(exceptionResponse, HttpStatus.BAD_REQUEST);
	}	

}
