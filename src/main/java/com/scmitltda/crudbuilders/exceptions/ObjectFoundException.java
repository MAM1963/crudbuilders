package com.scmitltda.crudbuilders.exceptions;

public class ObjectFoundException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public ObjectFoundException(String msg) {
		super(msg);
	}

}
