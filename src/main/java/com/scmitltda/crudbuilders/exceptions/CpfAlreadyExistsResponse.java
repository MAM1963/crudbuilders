package com.scmitltda.crudbuilders.exceptions;

public class CpfAlreadyExistsResponse {
	private String cpf;

	public CpfAlreadyExistsResponse(String cpf) {
		this.cpf = cpf;
	}

	public String getCpf() {
		return cpf;
	}

	public void setCpf(String cpf) {
		this.cpf = cpf;
	}
}
