package com.scmitltda.crudbuilders.resources;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.scmitltda.crudbuilders.domain.Cliente;
import com.scmitltda.crudbuilders.resources.utils.URL;
import com.scmitltda.crudbuilders.services.ClienteService;
import com.scmitltda.crudbuilders.services.MapValidationErrorService;

@RestController
@RequestMapping(value="/api/scrudbuilders/cliente")
public class ClienteResource {
	
	@Autowired
	private ClienteService clienteService;
	
	@Autowired
	private MapValidationErrorService mapValidationErrorService;

	@PostMapping("/criar")
	public ResponseEntity<?> save(@Valid @RequestBody Cliente cliente, BindingResult result) {
		
		ResponseEntity<?> errorMap = mapValidationErrorService.mapValidationService(result);
		
		if (errorMap != null) return errorMap;

		Cliente newCliente = clienteService.save(cliente);
		
		return new ResponseEntity<Cliente>(newCliente, HttpStatus.CREATED);
	}
	
	@GetMapping("/listar")
	public Iterable<Cliente> listarTodos() {
		return clienteService.findAll();
	}
	
	@DeleteMapping("/excluir/{cpf}")
	public ResponseEntity<?> exluirClientePorCpf(@PathVariable String cpf) {
		String cpfSemAspas = cpf.replaceAll("\"", "");
		clienteService.excluirClientePorCpf(cpfSemAspas);
		return new ResponseEntity<String>("Cliente com CPF '" + cpfSemAspas + 
				"' foi excluído", HttpStatus.OK);
	}
	
	@PutMapping("/alterar/{cpf}")
	public ResponseEntity<?> alterarClientePorCpf(@Valid @RequestBody Cliente cliente, 
												  BindingResult result,
												  @PathVariable String cpf) {
		
		ResponseEntity<?> errorMap = mapValidationErrorService.mapValidationService(result);
		
		if (errorMap != null) {
			return errorMap;
		}
		
		String cpfSemAspas = cpf.replaceAll("\"", "");
		
		Cliente clienteUpdated = clienteService.altararClientePorCpf(cliente, cpfSemAspas);
		
		return new ResponseEntity<Cliente>(clienteUpdated, HttpStatus.OK);
	}
	
	@PatchMapping("/atualizar/{cpf}")
	public ResponseEntity<?> alterarNomeEDataNascimento(
												  @Valid @RequestBody Cliente cliente, 
												  BindingResult result,
												  @PathVariable String cpf) {
		
		ResponseEntity<?> errorMap = mapValidationErrorService.mapValidationService(result);
		
		if (errorMap != null) {
			return errorMap;
		}
		
		String cpfSemAspas = cpf.replaceAll("\"", "");
		
		Cliente clienteUpdated = clienteService.alterarNomeEDataNascimento(cliente, cpfSemAspas);
		
		return new ResponseEntity<Cliente>(clienteUpdated, HttpStatus.OK);
	}
	
	@GetMapping("/buscarPorNome")
	public ResponseEntity<Page<Cliente>> findPageByNome(
			@RequestParam(value="nome", defaultValue="") String nome, 
			@RequestParam(value="page", defaultValue="0") Integer page, 
			@RequestParam(value="linesPerPage", defaultValue="24") Integer linesPerPage, 
			@RequestParam(value="orderBy", defaultValue="nome") String orderBy, 
			@RequestParam(value="direction", defaultValue="ASC") String direction) {
		String nomeDecoded = URL.decodeParam(nome);
		Page<Cliente> clientes = clienteService.buscarClientePorNome(nomeDecoded, page, linesPerPage, orderBy, direction);
		return ResponseEntity.ok().body(clientes);
	} 
	
	@GetMapping("/buscarPorCpf")
	public ResponseEntity<Page<Cliente>> findPageByCpf(
			@RequestParam(value="cpf", defaultValue="") String cpf, 
			@RequestParam(value="page", defaultValue="0") Integer page, 
			@RequestParam(value="linesPerPage", defaultValue="24") Integer linesPerPage, 
			@RequestParam(value="orderBy", defaultValue="nome") String orderBy, 
			@RequestParam(value="direction", defaultValue="ASC") String direction) {
		String cpfDecoded = URL.decodeParam(cpf);
		Page<Cliente> clientes = clienteService.buscarClientePorCpf(cpfDecoded, page, linesPerPage, orderBy, direction);
		return ResponseEntity.ok().body(clientes);
	} 
	
	@GetMapping("/buscarPorNomeECpf")
	public ResponseEntity<Page<Cliente>> findPageByNomeAndCpf(
			@RequestParam(value="nome", defaultValue="") String nome, 
			@RequestParam(value="cpf", defaultValue="") String cpf, 
			@RequestParam(value="page", defaultValue="0") Integer page, 
			@RequestParam(value="linesPerPage", defaultValue="24") Integer linesPerPage, 
			@RequestParam(value="orderBy", defaultValue="nome") String orderBy, 
			@RequestParam(value="direction", defaultValue="ASC") String direction) {
		String nomeDecoded = URL.decodeParam(nome);
		String cpfDecoded = URL.decodeParam(cpf);
		Page<Cliente> clientes = clienteService.buscarClientePorNomeECpf(nomeDecoded, cpfDecoded, page, linesPerPage, orderBy, direction);
		return ResponseEntity.ok().body(clientes);
	} 


}
